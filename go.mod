module gitlab.inria.fr/osallou/gozilla-lfs

go 1.13

require (
	github.com/DataDog/zstd v1.4.5 // indirect
	github.com/armon/go-metrics v0.3.3 // indirect
	github.com/gophercloud/gophercloud v0.9.0 // indirect
	github.com/hashicorp/consul/api v1.4.0 // indirect
	github.com/hashicorp/go-hclog v0.12.2 // indirect
	github.com/hashicorp/go-immutable-radix v1.2.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/hashicorp/serf v0.9.0 // indirect
	github.com/klauspost/compress v1.10.4 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/zerolog v1.18.0
	gitlab.inria.fr/osallou/gozilla-lib v0.0.0-20200331131016-d4093b6ec342
	go.mongodb.org/mongo-driver v1.3.2 // indirect
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200408040146-ea54a3c99b9b // indirect
)
