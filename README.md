# Goz-lfs

## About

Goz-lfs is a custom Git LFS agent to store large/binary files on Gozilla (https://gitlab.inria.fr/osallou/gozilla) in a git repository.

Gozilla supports unauthenticated access for public repo.

A Gozilla repository must be created prior to pushing LFS objects.

## Usage

To use Goz-lfs, you need to have Git LFS installed and goz-lfs binary installed on your system.

Then, after cloning or creating your local git repo, setup lfs with goz-lfs for using custom transfer agent goz-lfs

    git config --add lfs.customtransfer.goz-lfs.path goz-lfs
    git config --add lfs.customtransfer.goz-lfs.args "--subject *subjectid* --repo *repo_for_files* --url *gozilla_url* --user *gozilla_user* --apikey *gozilla_apikey*"
    git config --add lfs.standalonetransferagent goz-lfs

Example for .git/config with gozilla for upload/download

    [lfs "customtransfer.goz-lfs"]
        path = path_to_binary/goz-lfs
        args = --subject *userid* --repo *repo_for_files* --url *gozilla_url* --user *gozilla_user* --apikey *gozilla_apikey*
    [lfs]
        standalonetransferagent = goz-lfs

Example for .git/config with gozilla for download only (git lfs pull, etc.)

    [lfs "customtransfer.goz-lfs"]
        path = path_to_binary/goz-lfs
        args = --subject *userid* --repo *repo_for_files* --url *gozilla_url*
    [lfs]
        standalonetransferagent = goz-lfs

## License (MIT)

Copyright 2020 Olivier Sallou, IRISA, Universite Rennes 1

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Credits

* lfs-utils contains code extracted from https://github.com/sinbad/lfs-folderstore/blob/master/service/service.gon, Copyright 2018 Steve Streeting, License: MIT
