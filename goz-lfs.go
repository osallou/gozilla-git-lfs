package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/rs/zerolog"

	goz "gitlab.inria.fr/osallou/gozilla-lib"
)

// GozPackage is used package name for git lfs
const GozPackage = "goz-lfs"

// GozVersion is used version name for git lfs
const GozVersion = "origin"

// Version client version
var Version string

// Options cmd line/env args
type Options struct {
	User    string
	APIKEY  string
	URL     string
	Subject string
	Repo    string
}

// GozConfig gozilla related config
type GozConfig struct {
	StorageType goz.StorageType
	GozServer   string
	UID         string
	APIKEY      string
	APIVersion  string

	Subject string
	Repo    string
	Package string
	Version string
}

func getConfig(options Options) (*GozConfig, error) {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api", options.URL), nil)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		fmt.Printf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			fmt.Printf("Error: %s", body)
		}
		return nil, fmt.Errorf("List command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var cfg map[string]interface{}
	err = json.Unmarshal(body, &cfg)

	gozCfg := GozConfig{
		APIVersion:  cfg["apiVersion"].(string),
		StorageType: goz.StorageType(cfg["storage"].(float64)),
		UID:         options.User,
		APIKEY:      options.APIKEY,
		GozServer:   options.URL,

		Subject: options.Subject,
		Repo:    options.Repo,
		Package: GozPackage,
		Version: GozVersion,
	}
	return &gozCfg, err
}

// newRawStorageFileHandler gets a pointer to swift file handler
func newRawStorageFileHandler(config GozConfig) StorageFileHandler {
	return &RawStorageFileHandler{SwiftStorageFileHandler{
		Config: config,
	},
	}
}

// newSwiftStorageFileHandler gets a pointer to swift file handler
func newSwiftStorageFileHandler(config GozConfig) StorageFileHandler {
	return &SwiftStorageFileHandler{
		Config: config,
		Options: SwiftOptions{
			Size: 5000000000,
		},
	}
}

// newStorageFileHandler returns a StorageFileHandler according to config storage type
func newStorageFileHandler(config GozConfig, subject string) StorageFileHandler {
	if config.StorageType == goz.SwiftStorage {
		return newSwiftStorageFileHandler(config)
	}
	if config.StorageType == goz.LocalStorage {
		return newRawStorageFileHandler(config)
	}
	return nil
}

func retrieve(options Options, gitDir string, oid string, size int64, a *Action, writer, errWriter *bufio.Writer) {
	cfg, cfgErr := getConfig(options)
	if cfgErr != nil {
		sendTransferError(oid, 13, fmt.Sprintf("Cannot get config from server: %v", cfgErr), writer, errWriter)
	}

	storeFileHandler := newStorageFileHandler(*cfg, options.Subject)

	if storeFileHandler == nil {
		sendTransferError(oid, 13, fmt.Sprintf("failed to init remote storage: %v", fmt.Errorf("storage handler error")), writer, errWriter)
		return
	}

	remoteFrom := storagePath(oid)
	to := downloadTempPath(gitDir, oid)
	from := fmt.Sprintf("%s/%s/%s/%s/%s", options.Subject, options.Repo, GozPackage, GozVersion, remoteFrom)
	progressHandler := func(total int64, read int) {
		progress := &ProgressResponse{Event: "progress", Oid: oid, BytesSoFar: total, BytesSinceLast: read}
		sendResponse(progress, writer, errWriter)
	}
	downloadErr := storeFileHandler.Download(from, to, progressHandler)

	if downloadErr != nil {
		_, err := os.Stat(to)
		if err == nil {
			os.Remove(to)
		}
		sendTransferError(oid, 17, fmt.Sprintf("failed to transfer from remote storage %q: %v", to, downloadErr), writer, errWriter)
		return
	}

	complete := &TransferResponse{Event: "complete", Oid: oid, Path: to, Error: nil}
	err := sendResponse(complete, writer, errWriter)
	if err != nil {
		writeToStderr(fmt.Sprintf("Unable to send completion message: %v\n", err), errWriter)
	}

}

func store(options Options, oid string, size int64, a *Action, fromPath string, writer, errWriter *bufio.Writer) {
	cfg, cfgErr := getConfig(options)
	if cfgErr != nil {
		sendTransferError(oid, 13, fmt.Sprintf("Cannot get config from server %q: %v", fromPath, cfgErr), writer, errWriter)
	}

	storeFileHandler := newStorageFileHandler(*cfg, options.Subject)

	if storeFileHandler == nil {
		sendTransferError(oid, 13, fmt.Sprintf("failed to init remote storage %q: %v", fromPath, fmt.Errorf("storage handler error")), writer, errWriter)
		return
	}

	destPath := storagePath(oid)
	to := fmt.Sprintf("%s/%s/%s/%s/%s", options.Subject, options.Repo, GozPackage, GozVersion, destPath)
	progressHandler := func(total int64, read int) {
		progress := &ProgressResponse{Event: "progress", Oid: oid, BytesSoFar: total, BytesSinceLast: read}
		sendResponse(progress, writer, errWriter)
	}
	uploadErr := storeFileHandler.Upload(fromPath, to, progressHandler)
	if uploadErr != nil {
		sendTransferError(oid, 17, fmt.Sprintf("failed to transfer to remote storage %q: %v", fromPath, uploadErr), writer, errWriter)
		return
	}

	complete := &TransferResponse{Event: "complete", Oid: oid, Error: nil}
	err := sendResponse(complete, writer, errWriter)
	if err != nil {
		writeToStderr(fmt.Sprintf("Unable to send completion message: %v\n", err), errWriter)
	}

}

func serve(options Options, stdin io.Reader, stdout, stderr io.Writer) {

	scanner := bufio.NewScanner(stdin)
	writer := bufio.NewWriter(stdout)
	errWriter := bufio.NewWriter(stderr)

	gitDir, err := gitDir()
	if err != nil {
		writeToStderr(fmt.Sprintf("Unable to retrieve git dir: %v\n", err), errWriter)
		return
	}

	for scanner.Scan() {
		line := scanner.Text()
		var req Request

		if err := json.Unmarshal([]byte(line), &req); err != nil {
			writeToStderr(fmt.Sprintf("Unable to parse request: %v\n", line), errWriter)
			continue
		}

		switch req.Event {
		case "init":
			resp := &InitResponse{}
			writeToStderr(fmt.Sprintf("Initialised goz-lfs custom adapter for %s\n", req.Operation), errWriter)
			sendResponse(resp, writer, errWriter)
		case "download":
			retrieve(options, gitDir, req.Oid, req.Size, req.Action, writer, errWriter)
		case "upload":
			store(options, req.Oid, req.Size, req.Action, req.Path, writer, errWriter)
		case "terminate":
			break
		}
	}

}

func main() {

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") == "1" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	var subject string
	var repo string
	var url string
	var user string
	var apiKey string
	var showVersion bool

	flag.StringVar(&subject, "subject", "", "subject identifier")
	flag.StringVar(&repo, "repo", "", "repository identifier")
	flag.StringVar(&user, "user", "", "login identifier")
	flag.StringVar(&apiKey, "apikey", "", "Authentication API Key")
	flag.StringVar(&url, "url", "", "URL to Gozilla host")
	flag.BoolVar(&showVersion, "version", false, "show client version")
	flag.Parse()

	if s := os.Getenv("GOZ_USER"); s != "" {
		user = s
	}
	if s := os.Getenv("GOZ_APIKEY"); s != "" {
		apiKey = s
	}
	if s := os.Getenv("GOZ_URL"); s != "" {
		url = s
	}
	if s := os.Getenv("GOZ_SUBJECT"); s != "" {
		subject = s
	}
	if s := os.Getenv("GOZ_REPO"); s != "" {
		repo = s
	}

	if showVersion {
		fmt.Printf("Version: %s\n", Version)
		os.Exit(0)
	}

	options := Options{
		User:    user,
		APIKEY:  apiKey,
		URL:     url,
		Subject: subject,
		Repo:    repo,
	}
	serve(options, os.Stdin, os.Stdout, os.Stderr)
}
