package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

/*
Below code extracted from Code from https://github.com/sinbad/lfs-folderstore/blob/master/service/service.go
Copyright 2018 Steve Streeting
License: MIT
*/

func downloadTempPath(gitDir string, oid string) string {
	// Download to a subfolder of repo so that git-lfs's final rename can work
	// It won't work if TEMP is on another drive otherwise
	// basedir is the objects/ folder, so use the tmp folder
	tmpfld := filepath.Join(gitDir, "lfs", "tmp")
	os.MkdirAll(tmpfld, os.ModePerm)
	return filepath.Join(tmpfld, fmt.Sprintf("%v.tmp", oid))
}

func storagePath(oid string) string {
	// Use same folder split as lfs itself
	fld := filepath.Join(oid[0:2], oid[2:4])
	return filepath.Join(fld, oid)
}

func gitDir() (string, error) {
	cmd := newCmd("git", "rev-parse", "--git-dir")
	out, err := cmd.Output()
	if err != nil {
		return "", fmt.Errorf("Failed to call git rev-parse --git-dir: %v %v", err, string(out))
	}
	path := strings.TrimSpace(string(out))
	return absPath(path)

}

func absPath(path string) (string, error) {
	if len(path) > 0 {
		path, err := filepath.Abs(path)
		if err != nil {
			return "", err
		}
		return filepath.EvalSymlinks(path)
	}
	return "", nil
}

func newCmd(name string, arg ...string) *exec.Cmd {
	cmd := exec.Command(name, arg...)
	return cmd
}

// writeToStderr is for when you need to print extra information
func writeToStderr(msg string, errWriter *bufio.Writer) {
	if !strings.HasSuffix(msg, "\n") {
		msg = msg + "\n"
	}
	errWriter.WriteString(msg)
	errWriter.Flush()
}

// Header struct
type Header struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

// Action struct
type Action struct {
	Href      string            `json:"href"`
	Header    map[string]string `json:"header,omitempty"`
	ExpiresAt time.Time         `json:"expires_at,omitempty"`
}

// TransferError struct
type TransferError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// Request struct which can accept anything
type Request struct {
	Event               string  `json:"event"`
	Operation           string  `json:"operation"`
	Concurrent          bool    `json:"concurrent"`
	ConcurrentTransfers int     `json:"concurrenttransfers"`
	Oid                 string  `json:"oid"`
	Size                int64   `json:"size"`
	Path                string  `json:"path"`
	Action              *Action `json:"action"`
}

// InitResponse with response for init
type InitResponse struct {
	Error *TransferError `json:"error,omitempty"`
}

// TransferResponse generic transfer response
type TransferResponse struct {
	Event string         `json:"event"`
	Oid   string         `json:"oid"`
	Path  string         `json:"path,omitempty"` // always blank for upload
	Error *TransferError `json:"error,omitempty"`
}

// ProgressResponse blah
type ProgressResponse struct {
	Event          string `json:"event"`
	Oid            string `json:"oid"`
	BytesSoFar     int64  `json:"bytesSoFar"`
	BytesSinceLast int    `json:"bytesSinceLast"`
}

// sendResponse sends an actual response to lfs
func sendResponse(r interface{}, writer, errWriter *bufio.Writer) error {
	b, err := json.Marshal(r)
	if err != nil {
		return err
	}
	// Line oriented JSON
	b = append(b, '\n')
	_, err = writer.Write(b)
	if err != nil {
		return err
	}
	writer.Flush()
	writeToStderr(fmt.Sprintf("Sent message %v", string(b)), errWriter)
	return nil
}

// sendTransferError sends an error back to lfs
func sendTransferError(oid string, code int, message string, writer, errWriter *bufio.Writer) {
	resp := &TransferResponse{"complete", oid, "", &TransferError{code, message}}
	err := sendResponse(resp, writer, errWriter)
	if err != nil {
		writeToStderr(fmt.Sprintf("Unable to send transfer error: %v\n", err), errWriter)
	}
}

// sendProgress reports progress on operations
func sendProgress(oid string, bytesSoFar int64, bytesSinceLast int, writer, errWriter *bufio.Writer) {
	resp := &ProgressResponse{"progress", oid, bytesSoFar, bytesSinceLast}
	err := sendResponse(resp, writer, errWriter)
	if err != nil {
		writeToStderr(fmt.Sprintf("Unable to send progress update: %v\n", err), errWriter)
	}
}

// End code from https://github.com/sinbad/lfs-folderstore/blob/master/service/service.go
