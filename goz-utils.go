package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	goz "gitlab.inria.fr/osallou/gozilla-lib"
)

// SwiftOptions options to access swift content
type SwiftOptions struct {
	Server        string
	Bucket        string
	File          string
	ObjectName    string
	Size          int64
	Prefix        string
	LeaveSegments bool
	Meta          map[string]string
}

// SwiftFile describe a swift object
type SwiftFile struct {
	Hash         string
	LastModified string `json:"last_modified"`
	Bytes        uint64
	Name         string
	ContentType  string `json:"content_type"`
}

// SwiftStorageFileHandler handles file upload/download with swift
type SwiftStorageFileHandler struct {
	Options SwiftOptions
	Config  GozConfig
}

// RawStorageFileHandler handles files with local storage app
type RawStorageFileHandler struct {
	SwiftStorageFileHandler
}

// Segment is a file segment for swift
type Segment struct {
	From int64
	Size int64
}

func fileSize(path string) int64 {
	fi, err := os.Stat(path)
	if err != nil {
		log.Error().Msgf("%s", err)
		return 0
	}
	return fi.Size()
}

// ProgressHandler is a function to handle progress
type ProgressHandler func(total int64, read int)

// StorageFileHandler interface to access storage files
type StorageFileHandler interface {
	Show(file string) (data goz.FileInfo, err error)
	Upload(from string, to string, handler ProgressHandler) error
	DeleteWithPrefix(prefix string) error
	DeleteFile(file string) error
	DownloadWithPrefix(prefix string, to string, handler ProgressHandler) error
	Download(from string, to string, handler ProgressHandler) error
	List(prefix string) ([]goz.FileObject, error)
}

// WriteCounter counts the number of bytes written to it. It implements to the io.Writer interface
// and we can pass this into io.TeeReader() which will report progress on each write cycle.
type WriteCounter struct {
	Total           int64
	ProgressHandler ProgressHandler
}

func (wc *WriteCounter) Write(p []byte) (int, error) {
	n := len(p)
	wc.Total += int64(n)
	wc.sendProgress(n)
	return n, nil
}

func (wc *WriteCounter) sendProgress(n int) {
	if wc.ProgressHandler == nil {
		return
	}
	wc.ProgressHandler(wc.Total, n)
}

// NewRawStorageFileHandler gets a pointer to swift file handler
func NewRawStorageFileHandler(config GozConfig) StorageFileHandler {
	return &RawStorageFileHandler{SwiftStorageFileHandler{
		Config: config,
	},
	}
}

// Upload TODO
func (s *RawStorageFileHandler) Upload(from string, to string, handler ProgressHandler) error {
	log.Debug().Msgf("Upload: %s => %s\n", from, to)
	url := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", to}
	log.Debug().Msgf("Call %s\n", strings.Join(url, "/"))
	fSize := fileSize(from)
	if fSize == 0 {
		return fmt.Errorf("file not found or empty %s", from)
	}

	err := s.upload(from, to)
	handler(fSize, 0)

	return err
}

func (s *RawStorageFileHandler) upload(from string, to string) error {

	fSize := fileSize(from)
	if fSize == 0 {
		return fmt.Errorf("file not found or empty %s", from)
	}

	segment := Segment{From: 0, Size: fSize}

	data, derr := os.Open(from)
	var body *bytes.Reader

	log.Debug().Msgf("File %s, Segment %d, %d", from, segment.From, segment.Size)
	data.Seek(segment.From, 0)
	byteData := make([]byte, segment.Size)
	data.Read(byteData)
	body = bytes.NewReader(byteData)

	if derr != nil {
		return derr
	}
	defer data.Close()

	client := &http.Client{}
	segmentTo := to

	segurl := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", segmentTo}
	log.Debug().Msgf("Call %s\n", strings.Join(segurl, "/"))

	req, _ := http.NewRequest("PUT", strings.Join(segurl, "/"), body)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	for m := range s.Options.Meta {
		req.Header.Add("X-Object-Meta-"+m, s.Options.Meta[m])
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Msgf("Failed to contact server %s\n", s.Config.GozServer)
		return err
	}
	if resp.StatusCode != 201 {
		log.Error().Msgf("Failed to upload file: %s", resp.Status)
	}
	return nil
}

// NewSwiftStorageFileHandler gets a pointer to swift file handler
func NewSwiftStorageFileHandler(config GozConfig) StorageFileHandler {
	return &SwiftStorageFileHandler{
		Config: config,
		Options: SwiftOptions{
			Size: 5000000000,
		},
	}
}

// Show TODO
func (s *RawStorageFileHandler) Show(path string) (data goz.FileInfo, err error) {
	client := &http.Client{}
	url := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", path}
	log.Debug().Msgf("Call head %s\n", strings.Join(url, "/"))

	req, _ := http.NewRequest("HEAD", strings.Join(url, "/"), nil)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Msgf("Failed to contact server %s\n", s.Config.GozServer)
		return data, err
	}
	defer resp.Body.Close()

	headers := resp.Header
	if _, ok := headers["Content-Length"]; ok {
		size, serr := strconv.Atoi(headers["Content-Length"][0])
		if serr == nil {
			data.Size = int64(size)
		}
	}
	if _, ok := headers["Last-Modified"]; ok {
		t, terr := http.ParseTime(headers["Last-Modified"][0])
		if terr == nil {
			data.LastUpdated = t.Unix()
		}
	}
	return data, nil
}

// Show TODO
func (s *SwiftStorageFileHandler) Show(path string) (data goz.FileInfo, err error) {
	client := &http.Client{}
	url := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", path}
	log.Debug().Msgf("Call head %s\n", strings.Join(url, "/"))

	req, _ := http.NewRequest("HEAD", strings.Join(url, "/"), nil)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Msgf("Failed to contact server %s\n", s.Config.GozServer)
		return data, err
	}
	defer resp.Body.Close()

	headers := resp.Header
	size, serr := strconv.Atoi(headers["Content-Length"][0])
	if serr == nil {
		data.Size = int64(size)
	}
	t, terr := http.ParseTime(headers["Last-Modified"][0])
	if terr == nil {
		data.LastUpdated = t.Unix()
	}
	data.MD5 = headers["ETag"][0]
	return data, nil
}

// head checks if remote file is a multi-part object, return manifest value
func (s *SwiftStorageFileHandler) head(path string) (string, error) {
	manifest := ""
	client := &http.Client{}
	url := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", path}
	log.Debug().Msgf("Call head %s\n", strings.Join(url, "/"))
	req, _ := http.NewRequest("HEAD", strings.Join(url, "/"), nil)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Msgf("Failed to contact server %s\n", s.Config.GozServer)
		return manifest, err
	}
	defer resp.Body.Close()
	manifest = resp.Header.Get("X-Object-Manifest")
	log.Debug().Msgf("Found old manifest %s", manifest)
	if resp.StatusCode != 200 {
		log.Debug().Msgf("Not available: %s\n", resp.Status)
		return manifest, fmt.Errorf("could not find manifest")
	}
	return manifest, nil
}

func (s *SwiftStorageFileHandler) uploadManifest(segmentPrefix string, path string) error {
	client := &http.Client{}
	url := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", path}
	log.Debug().Msgf("Call %s %s\n", s.Options.Bucket, strings.Join(url, "/"))
	log.Debug().Msgf("Set manifest %s", segmentPrefix)
	byteData := make([]byte, 0)
	req, _ := http.NewRequest("PUT", strings.Join(url, "/"), bytes.NewReader(byteData))
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	req.Header.Add("X-Object-Manifest", segmentPrefix)
	for m := range s.Options.Meta {
		log.Debug().Msgf("Add metadata %s: %s\n", m, s.Options.Meta[m])
		req.Header.Add("X-Object-Meta-"+m, s.Options.Meta[m])
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msgf("Failed to contact server %s\n", s.Config.GozServer)
		return err
	}
	if resp.StatusCode != 201 {
		log.Error().Msgf("Failed to upload file: %s", resp.Status)
		return fmt.Errorf("failed to upload file: %s", resp.Status)
	}
	log.Debug().Msgf("Manifest uploaded => %s", strings.Join(url, "/"))
	return nil

}

func (s *SwiftStorageFileHandler) uploadSegment(ch chan bool, segment Segment, single bool, from string, to string, handler ProgressHandler) {
	data, derr := os.Open(from)
	var body *bytes.Reader

	log.Debug().Msgf("File %s, Segment %d, %d", from, segment.From, segment.Size)
	data.Seek(segment.From, 0)
	byteData := make([]byte, segment.Size)
	data.Read(byteData)
	body = bytes.NewReader(byteData)

	if derr != nil {
		log.Error().Msgf("Failed to open file %s", s.Options.File)
		ch <- false
		return
	}
	defer data.Close()

	client := &http.Client{}
	segmentTo := to
	if !single {
		urlElt := strings.Split(to, "/")
		urlElt[0] = urlElt[0] + "_segments"
		segmentTo = strings.Join(urlElt, "/")
	}
	segurl := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", segmentTo}
	log.Debug().Msgf("Call %s\n", strings.Join(segurl, "/"))

	req, _ := http.NewRequest("PUT", strings.Join(segurl, "/"), body)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	for m := range s.Options.Meta {
		req.Header.Add("X-Object-Meta-"+m, s.Options.Meta[m])
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Msgf("Failed to contact server %s\n", s.Config.GozServer)
		ch <- false
		return
	}
	if resp.StatusCode != 201 {
		log.Error().Msgf("Failed to upload file: %s", resp.Status)
		ch <- false
		return
	}
	handler(segment.Size, 0)
	ch <- true
	return
}

// Upload TODO
func (s *SwiftStorageFileHandler) Upload(from string, to string, handler ProgressHandler) error {
	log.Debug().Msgf("Upload: %s => %s\n", from, to)
	url := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", to}
	log.Debug().Msgf("Call %s\n", strings.Join(url, "/"))
	fSize := fileSize(from)
	if fSize == 0 {
		return fmt.Errorf("file not found or empty %s", from)
	}

	// check if exists and was a x-object-manifest
	// if yes keep list and after upload, delete old segments
	// need to query files with prefix defined in manifest to delete them
	oldManifest, oldManifestErr := s.head(to)

	if fSize > s.Options.Size && s.Options.Size > 0 {
		log.Debug().Msg("File too big, segmenting it....")
		nbSegment := int64(math.Floor(float64(fSize)/float64(s.Options.Size)) + 1)
		start := int64(0)
		size := int64(0)
		ch := make(chan bool)
		uploadDone := int64(0)
		// s.Options.Bucket = s.Options.Bucket + "_segments"
		ts := time.Now().UnixNano()

		segmentPrefix := []string{to, strconv.FormatInt(ts, 10), strconv.FormatInt(fSize, 10)}
		for i := int64(0); i < nbSegment; i++ {
			segmentSize := s.Options.Size
			if i == nbSegment-1 {
				segmentSize = int64(fSize) - size // remaining
			}
			segment := Segment{From: start, Size: segmentSize}
			log.Debug().Msgf("create segment %d: %d [%d]", i, segment.From, segment.Size)
			index := fmt.Sprintf("%010d", i)
			segmentFileName := []string{to, strconv.FormatInt(ts, 10), strconv.FormatInt(fSize, 10), index}
			newObjectName := strings.Join(segmentFileName, "/")
			go s.uploadSegment(ch, segment, false, from, newObjectName, handler)
			start += segmentSize
			size += s.Options.Size
		}
		gotErrors := false
		for uploadDone < nbSegment {
			uploadRes := <-ch
			if !uploadRes {
				gotErrors = true
				log.Error().Msg("Failed to upload file segment\n")
			} else {
				log.Debug().Msg("Segment uploaded!")
			}
			uploadDone++
		}
		close(ch)
		if gotErrors {
			return fmt.Errorf("failed to upload some segments")
		}
		s.uploadManifest(strings.Join(segmentPrefix, "/"), to)

	} else {
		ch := make(chan bool)
		segment := Segment{From: 0, Size: fSize}
		go s.uploadSegment(ch, segment, true, from, to, handler)
		uploadRes := <-ch
		if !uploadRes {
			return fmt.Errorf("failed to upload file")
		}
		log.Debug().Msg("Uploaded!")
		close(ch)
	}

	if oldManifestErr == nil && s.Options.LeaveSegments == false {
		log.Debug().Msgf("Delete old segments")
		log.Debug().Msgf("List with manifest prefix in _segments")
		prefix := strings.Replace(oldManifest, s.Options.Bucket+"_segments/", "", -1)
		s.Options.Bucket = s.Options.Bucket + "_segments"
		oldFiles, _ := s.List(prefix)
		log.Debug().Msgf("Delete old segment files")
		for _, file := range oldFiles {
			log.Debug().Msgf("Delete segment %s\n", file.GetName())
			s.DeleteFile(file.GetName())
		}
	}
	return nil
}

// DeleteWithPrefix TODO
func (s *SwiftStorageFileHandler) DeleteWithPrefix(prefix string) error {
	files, err := s.List(prefix)
	if err != nil {
		return err
	}
	deleteSuccess := true
	for _, file := range files {
		delErr := s.DeleteFile(file.GetName())
		if delErr != nil {
			log.Error().Str("file", file.GetPath()).Msg("delete error")
			deleteSuccess = false
		}
	}
	if !deleteSuccess {
		return fmt.Errorf("got some delete errors")
	}
	return nil
}

// DeleteFile TODO
func (s *SwiftStorageFileHandler) DeleteFile(file string) error {
	log.Debug().Str("file", file).Msg("Deleting")
	client := &http.Client{}
	discard := fmt.Sprintf("%s/%s/%s/%s/%s", s.Config.Subject, s.Config.Repo, s.Config.Package, s.Config.Version, file)

	req, _ := http.NewRequest("DELETE", fmt.Sprintf("%s/api/v1.0/%s/%s", s.Config.GozServer, "content", discard), nil)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Msg(err.Error())
		return fmt.Errorf("failed to delete %s", file)
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("failed to delete %s, code: %d", file, resp.StatusCode)
	}
	log.Debug().Str("file", file).Msg("deleted")
	return nil
}

// DownloadWithPrefix TODO
func (s *SwiftStorageFileHandler) DownloadWithPrefix(prefix string, to string, progressHandler ProgressHandler) error {
	files, err := s.List(prefix)
	if err != nil {
		return err
	}
	downloadSuccess := true
	for _, file := range files {
		downErr := s.Download(file.GetName(), strings.Join([]string{to, file.GetName()}, "/"), progressHandler)
		if downErr != nil {
			log.Error().Str("file", file.GetPath()).Msg("download error")
			downloadSuccess = false
		}
	}
	if !downloadSuccess {
		return fmt.Errorf("got some download errors")
	}
	return nil
}

// Download TODO
func (s *SwiftStorageFileHandler) Download(from string, to string, progressHandler ProgressHandler) error {
	client := &http.Client{}

	url := []string{s.Config.GozServer, "api", s.Config.APIVersion, "content", from}
	log.Debug().Msgf("Call %s\n", strings.Join(url, "/"))
	req, _ := http.NewRequest("GET", strings.Join(url, "/"), nil)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	req.Header.Add("Accept", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Msgf("Failed to contact server %s\n", s.Config.GozServer)
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 && resp.StatusCode != 204 {
		log.Error().Msgf("Error: %s\n", resp.Status)
		return fmt.Errorf("error: %s", resp.Status)
	}
	if resp.StatusCode == 204 {
		log.Error().Msg("No content\n")
		return fmt.Errorf("no content")
	}
	mkerr := os.MkdirAll(filepath.Dir(to), 0755)
	if mkerr != nil {
		log.Error().Msgf("Error: %s", mkerr)
		return mkerr
	}
	out, err := os.Create(to)
	if err != nil {
		log.Error().Msgf("Error: %s", err)
		return err
	}
	defer out.Close()
	//_, err = io.Copy(out, resp.Body)
	counter := &WriteCounter{
		ProgressHandler: progressHandler,
	}
	if _, err = io.Copy(out, io.TeeReader(resp.Body, counter)); err != nil {
		out.Close()
		return err
	}
	return nil
}

// List TODO
func (s *SwiftStorageFileHandler) List(prefix string) ([]goz.FileObject, error) {

	client := &http.Client{}
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s/api/v1.0/%s/%s/%s/%s/versions/%s", s.Config.GozServer, "packages", s.Config.Subject, s.Config.Repo, s.Config.Package, s.Config.Version), nil)
	if s.Config.APIKEY != "" {
		req.Header.Set("X-GOZ-USER", s.Config.UID)
		req.Header.Set("X-GOZ-APIKEY", s.Config.APIKEY)
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		log.Error().Msgf("Invalid response code: %d\n", resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			log.Error().Msgf("Error: %s", body)
		}
		return nil, fmt.Errorf("Show command failed")
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	type PackageShowResult struct {
		Subject string              `json:"subject"`
		Repo    string              `json:"repo"`
		Package goz.Package         `json:"package"`
		Version goz.PackageVersion  `json:"version"`
		Files   []goz.RawFileObject `json:"files"`
	}
	var packsResult PackageShowResult
	err = json.Unmarshal(body, &packsResult)
	if err != nil {
		return nil, err
	}
	files := make([]goz.FileObject, len(packsResult.Files))
	for i, file := range packsResult.Files {
		if prefix != "" {
			if !strings.HasPrefix(file.GetName(), prefix) {
				continue
			}
		}
		files[i] = &file
	}
	return files, nil
}
